terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.45.0"
    }
  }
}

provider "aws" {
  region = "eu-north-1"
}

resource "aws_key_pair" "ansible_key" {
  key_name   = "ansible_key"
  public_key = file("${var.public_key_path}") # Path to your SSH public key
}

resource "tls_private_key" "ed25519_ansible" {
  algorithm = "ED25519"
}

resource "aws_security_group" "ansible_sg" {
  name        = "ansible_sg"
  description = "Security group for Ansible control node to allow SSH"
  vpc_id      = var.vpc_id

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ansible_server" {
  ami             = "ami-0705384c0b33c194c" # Ubuntu Server 24.04
  subnet_id       = var.ansible_subnet_id
  instance_type   = "t3.micro"
  key_name        = aws_key_pair.ansible_key.key_name
  vpc_security_group_ids = [aws_security_group.ansible_sg.id]
  associate_public_ip_address = true
  user_data = <<-EOF
#!/bin/bash
echo "${tls_private_key.ed25519_ansible.private_key_pem}" > /home/ubuntu/.ssh/ansible_key
chmod 600 /home/ubuntu/.ssh/ansible_key
echo "Host gitlab.com
  IdentityFile /home/ubuntu/.ssh/ansible_key
  StrictHostKeyChecking no" > /home/ubuntu/.ssh/config
chown -R ubuntu:ubuntu /home/ubuntu/.ssh
EOF

  tags = {
    Name = "AnsibleControlNode"
  }
}
