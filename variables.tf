variable "public_key_path" {
  description = "Path to the public key to be used for SSH access to the EC2 instance."
  type        = string
}

variable "ansible_subnet_id" {
  description = "The subnet id to deploy Ansible server in"
  type = string
}

variable "vpc_id" {
  description = "VPC id to deploy Ansible server in"
  type = string
}
